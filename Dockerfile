FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y curl \
    && curl -sL https://github.com/hadolint/hadolint/releases/latest/download/hadolint-Linux-x86_64 -o /bin/hadolint \
    && chmod +x /bin/hadolint

ENTRYPOINT [""]
